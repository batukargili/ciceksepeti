package ciceksepeti1;

import java.awt.image.AreaAveragingScaleFilter;
import java.io.File;

import java.io.FileNotFoundException;

import java.util.ArrayList;

import java.util.List;

import java.util.Scanner;





public class SiparisBayi {

	public static void main(String[] args) throws FileNotFoundException {

		//read bayi.csv and fill 2D array
		Scanner scanner = new Scanner(new File("/Users/batukargili/eclipse-workspace/ciceksepeti1/src/ciceksepeti1/bayi.csv"));
		scanner.useDelimiter(";");
		List<String> temps = new ArrayList<String>(); 

		while(scanner.hasNext()){
			temps.add(scanner.next());
		}
		scanner.close();

		String[] tempArray =temps.toArray(new String[0]);
		String bayi[][] = new String[4][4];		
		for(int i=0; i<4;i++)
			for(int j=0;j<4;j++)
				bayi[i][j] = tempArray[(i*4) + j];


		//read siparis.csv and fill 2D array
		Scanner scanner1 = new Scanner(new File("/Users/batukargili/eclipse-workspace/ciceksepeti1/src/ciceksepeti1/siparis.csv"));
		scanner1.useDelimiter(";");
		List<String> temps1 = new ArrayList<String>(); 

		while(scanner1.hasNext()){
			temps1.add(scanner1.next());
		}
		scanner1.close();

		String[] tempArray1 =temps1.toArray(new String[0]);
		String siparis[][] = new String[101][4];
		for(int i=0; i<101;i++)
			for(int j=0;j<4;j++)
				siparis[i][j] = tempArray1[(i*4) + j];
		List<SiparisNode> arrayKırmızı = new ArrayList<SiparisNode>();
		List<SiparisNode> arrayYeşil = new ArrayList<SiparisNode>();
		List<SiparisNode> arrayMavi = new ArrayList<SiparisNode>();
		//kırmızı 1.satır [1][1] ve [1][2], yeşil 2.satır [2][1] ve [2][2], mavi 3.satır [3][1] ve [3][2] 
		double max =999.0; //en kısa mesafeyi bulurken kıyaslama için kullanacağız
		int tempBayi = 0;
		double[] depolama = new double[3];
		for (int i = 1; i < 101; i++) { //her sipariş için döner
			//her sipariş için node oluştur onu da ait oldukları bayinin arrayine ekle
			SiparisNode siparis_no = new SiparisNode(Integer.parseInt(siparis[i][0].substring(2)), Double.parseDouble(siparis[i][1]), Double.parseDouble(siparis[i][2]),0);
			for (int j = 1; j <4; j++) { //her bayi için döner
				double distance = distance(Double.parseDouble(bayi[j][1]),Double.parseDouble(bayi[j][2]),
						Double.parseDouble(siparis[i][1]),Double.parseDouble(siparis[i][2]));
				depolama[j-1] = distance; // bayilerin kotaları dolarsa eskilerden en küçük olanı seçeceğiz
				if(distance<max){
					max=distance;
					tempBayi=j;
				}


				if(j == 3){
					//3 durum var 
					if(tempBayi==1){ //kırmızı bayi ise
						if(arrayKırmızı.size()<30) 
							arrayKırmızı.add(siparis_no);
						//eğer kırmızının kotası 30 ise
						if(arrayKırmızı.size()==30){ //sipariş yeşil ve mavi içinden en düşük mesafeye sahip bayiye atanır
							//depolama arrayinden seç
							if(depolama[1]>depolama[2]){//maviye daha yakınsa yeşile göre
								if(arrayMavi.size()<80)
									arrayMavi.add(siparis_no);
								else
									arrayYeşil.add(siparis_no);

							}
							else if (depolama[1]<depolama[2]){//yeşile daha yakınsa maviye göre
								if(arrayYeşil.size()<50)
									arrayYeşil.add(siparis_no);	
								else
									arrayMavi.add(siparis_no);
							}
						}
					}
					else if(tempBayi==2){ //yeşil bayi ise
						if(arrayYeşil.size()<50) 
							arrayYeşil.add(siparis_no);
						if(arrayYeşil.size()==50){ 
							if(depolama[0]>depolama[2])//maviye daha yakınsa kırmızıya göre
							{
								if(arrayMavi.size()<80)
									arrayMavi.add(siparis_no);
								else
									arrayKırmızı.add(siparis_no);
							}
							else if (depolama[0]<depolama[2]){ //kırmızıya daha yakınsa maviye göre
								if(arrayKırmızı.size()<30)
									arrayKırmızı.add(siparis_no);
								else
									arrayMavi.add(siparis_no);
							}
						}
					}
					else if(tempBayi==3){//mavi bayi ise
						if(arrayMavi.size()<80) 
							arrayMavi.add(siparis_no);
						if(arrayMavi.size()==80){ 
							if(depolama[0]>depolama[1]){//yeşile daha yakınsa kırmızıya göre
								if(arrayYeşil.size()<50)
									arrayYeşil.add(siparis_no);
								else
									arrayKırmızı.add(siparis_no);
							}
							else if (depolama[0]<depolama[1]){//kırmızıya daha yakınsa yeşile göre
								if(arrayKırmızı.size()<30)
									arrayKırmızı.add(siparis_no);
								else
									arrayYeşil.add(siparis_no);
							}

						}
					}
				}
			}//bayi loop
			max=999;
		}//siparis loop
	
		System.out.println("kırmızı bayinin yollayacağı siparis sayisi : "+arrayKırmızı.size());
		System.out.println("yeşil bayinin yollayacağı siparis sayisi : "+arrayYeşil.size());
		System.out.println("mavi bayinin yollayacağı siparis sayisi : "+arrayMavi.size());
		
		
		
		

		System.out.println("--------");
		
		Route(arrayKırmızı,"Kırmızı");
		Route(arrayYeşil,"Yeşil");
		Route(arrayMavi,"Mavi");
//		
//		Aşagıdaki 3 tane döngü haritaya yollanacak veriler için yazıldı(Harita https://www.darrinward.com/lat-long/ sitesinde oluşturuldu)
		for (int i = 0; i < arrayKırmızı.size(); i++) {
			System.out.println(arrayKırmızı.get(i).getLatitude()+","+arrayKırmızı.get(i).getLongitude()+","+arrayKırmızı.get(i).getId()+",#DC143C");
		}
		for (int i = 0; i < arrayYeşil.size(); i++) {
			System.out.println(arrayYeşil.get(i).getLatitude()+","+arrayYeşil.get(i).getLongitude()+","+arrayYeşil.get(i).getId()+",#00FF00");
		}
		for (int i = 0; i < arrayMavi.size(); i++) {
			System.out.println(arrayMavi.get(i).getLatitude()+","+arrayMavi.get(i).getLongitude()+","+arrayMavi.get(i).getId()+",#00BFFF");
		}
	}
	
	
	public static void Route(List<SiparisNode> routes,String bayi){
		int temp = 0,temp2=0;
		double maximum =999.0,tempdist=0.0;
		List<Integer> routeId = new ArrayList<Integer>(); //bayinin sırasıyla uğrayacağı siparişlerin idleri tutuluyor
		List<Double> route_dist = new ArrayList<Double>(); //direk mesafeleri toplaya toplaya gidersek bu Arraylist silinebilir, şimdilik kalsın
		for (int i = 0; i < routes.size(); i++) {
			for (int j = 0; j < routes.size(); j++) { 
				if(i!=j && routes.get(j).getChecked()!=1){
					double dist = distance(routes.get(i).getLatitude(),routes.get(i).getLongitude(),routes.get(j).getLatitude(),routes.get(j).getLongitude());
				
					if(dist<maximum){
						
							maximum=dist;
							temp = j;
							temp2=routes.get(j).getId();
							tempdist=dist;
						
					}
				}
			}
			maximum=999;
			//temp değerindeki objenin checked değerini 1 yap çünkü ziyaret edildi
			routes.get(temp).setChecked(1);
			//ve o değeri route arrayine at
			routeId.add(temp2);
			route_dist.add(tempdist);
		}
		double toplam = 0.0;
		System.out.println(bayi);
		System.out.print("Rota: ");
		for (int i = 0; i < routeId.size(); i++) {
			System.out.print(routeId.get(i)+" - ");
		}
		System.out.println();
		System.out.print(bayi+" bayinin Toplam gittiği mesafe : ");
		for (int i = 0; i < route_dist.size(); i++) {
			toplam+=route_dist.get(i);
		}
		System.out.println(toplam + " km");
		System.out.println();
		
	}
	public static double distance(double startLat, double startLong,
            double endLat, double endLong) {
			final int R = 6371;
			double dLat  = Math.toRadians((endLat - startLat));
				double dLong = Math.toRadians((endLong - startLong));

					startLat = Math.toRadians(startLat);
					endLat   = Math.toRadians(endLat);

					double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
					double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

					return R * c; // <-- d
}
	  public static double haversin(double val) {
	        return Math.pow(Math.sin(val / 2), 2);
	    }

}