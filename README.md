#Veri Okuma
Çiçeksepeti bayilerin ve siparişlerin enlem ve boylamlarının bulunduğu xlst dosyalarını okuma maaliyetini düşürmek için csv ye dönüştürdük.
Bu verilerden Haversin Algoritmasıyla bayilerin ve siparişlerin birbirine olan uzunluklarını hesapladık

#Problem Analizi 
Beklenilen çözüm siparisleri bayilere verilen kotalar dahilinde en optimal şekilde dağıtmak olduğu için, 
siparisleri bayilere en yakın olucak şekilde kümeledik.
Kümeleme işleminden sonra implementasyonu kolay bir Shortest-Path Algoritması geliştirdik.

#Output
En son çıktı olarak bayilerin dağıtacağı toplam sipariş sayıları, dağıtım rotaları ve renklendirilmiş harita görüntüsü bulunmaktadır
Not: Haritada gösterim işlemini java'da geotools gibi librarylerin kullanım zorluğu ve tatmin etmiyen görüntüler çıkarttığı için
programımızdan çıkan verileri https://www.darrinward.com/lat-long/ sitesine yapıştırarak çıkarttık.
