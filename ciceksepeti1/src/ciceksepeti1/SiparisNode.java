package ciceksepeti1;


public class SiparisNode {
	
	
	public SiparisNode(int id, double latitude, double longitude,int checked) {
		super();
		this.id = id;
		this.latitude = latitude;
		this.longitude = longitude;
		this.checked = checked;
	}
	private int id;
	private double latitude;
	private double longitude;
	private int checked;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public int getChecked() {
		return checked;
	}
	public void setChecked(int checked) {
		this.checked = checked;
	}
	
	
}
